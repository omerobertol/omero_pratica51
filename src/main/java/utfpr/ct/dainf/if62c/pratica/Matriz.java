package utfpr.ct.dainf.if62c.pratica;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Omero Francisco Bertol (27/09/2016)
 */

public class Matriz {
    
    // a matriz representada por esta classe
    private final double[][] mat;
    
    /**
     * Construtor que aloca a matriz.
     * @param numLinhas
     * @param numColunas
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz(int numLinhas, int numColunas) throws MatrizInvalidaException {
      if ((numLinhas <= 0) || (numColunas <= 0))
         throw new MatrizInvalidaException(numLinhas, numColunas);
      
      mat = new double[numLinhas][numColunas];
    }
    
    /**
     * Retorna a matriz representada por esta classe.
     * @return A matriz representada por esta classe
     */
    public double[][] getMatriz() {
        return mat;
    }
    
    /**
     * Retorna a matriz transposta.
     * @return A matriz transposta.
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz getTransposta() throws MatrizInvalidaException {
        Matriz t = new Matriz(mat[0].length, mat.length);
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                t.mat[j][i] = mat[i][j];
            }
        }
        return t;
    }
    
    /**
     * Retorna a soma desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser somada
     * @return A soma das matrizes
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz soma(Matriz m) throws MatrizInvalidaException {  
      if ((mat.length != m.mat.length) || (mat[0].length != m.mat[0].length))
         throw new SomaMatrizesIncompativeisException(this, m);
        
      Matriz t = new Matriz(mat.length, mat[0].length);  
      
      for (int i = 0; i < mat.length; i++) {
        for (int j = 0; j < mat[i].length; j++) {
          t.mat[i][j] = mat[i][j] + m.mat[i][j];
        }
      }
        
      return t;
    }

    /**
     * Retorna o produto desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser multiplicada
     * @return O produto das matrizes
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz prod(Matriz m) throws MatrizInvalidaException {
      if (mat[0].length != m.mat.length)
         throw new ProdMatrizesIncompativeisException(this, m);
      
      Matriz t = new Matriz(mat.length, m.mat[0].length);
      int i, j, k;
      double sm;
        
      // número de colunas da primeira matriz (mat) igual ao número de colunas na segunda matriz (m)
      for (i=0; i<mat.length; i++) {
        for (j=0; j<m.mat[i].length; j++) {
          sm = 0;
          for (k=0; k<m.mat.length; k++) {
            sm = sm + (mat[i][k] * m.mat[k][j]);
          }
          t.mat[i][j] = sm;
        }
      }
        
      return t;        
    }

    /**
     * Retorna uma representação textual da matriz.
     * Este método não deve ser usado com matrizes muito grandes
     * pois não gerencia adequadamente o tamanho do string e
     * poderia provocar um uso excessivo de recursos.
     * @return Uma representação textual da matriz.
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (double[] linha: mat) {
            s.append("[ ");
            for (double x: linha) {
                s.append(x).append(" ");
            }
            s.append("]");
        }
        return s.toString();
    }
    
}