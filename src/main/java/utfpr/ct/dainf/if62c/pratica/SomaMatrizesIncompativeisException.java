package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Omero Francisco Bertol
*/

public class SomaMatrizesIncompativeisException extends MatrizesIncompativeisException {

  public SomaMatrizesIncompativeisException(Matriz m1, Matriz m2) {    
    super("Matrizes de %dx%d e %dx%d não podem ser somadas", m1, m2);
  }
   
}
