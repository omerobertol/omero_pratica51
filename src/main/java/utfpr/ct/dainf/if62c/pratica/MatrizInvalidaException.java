package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Omero Francisco Bertol
 */

public class MatrizInvalidaException extends Exception {
  private final int numLinhas; 
  private final int numColunas;

  public MatrizInvalidaException(int numLinhas, int numColunas) { 
    super(String.format("Matriz de %dx%d não pode ser criada",
      numLinhas, numColunas));
  
    this.numLinhas = numLinhas;
    this.numColunas = numColunas;
  }
  
  public int getNumLinhas() {
    return numLinhas;
  }

  public int getNumColunas() {
    return numColunas;
  }

}