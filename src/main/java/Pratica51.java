import utfpr.ct.dainf.if62c.pratica.Matriz;
import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;
import utfpr.ct.dainf.if62c.pratica.ProdMatrizesIncompativeisException;
import utfpr.ct.dainf.if62c.pratica.SomaMatrizesIncompativeisException;

/**
 *
 * @author Omero Francisco Bertol (27/09/2016)
 */

public class Pratica51 {

  public static void main(String[] args) throws MatrizInvalidaException {
    // a, b matrizes com dimensões válidas
    Matriz a = new Matriz(2, 2);
    Matriz b = new Matriz(2, 2);
      
    // c, d matrizes com dimensões inválidas
    Matriz c = new Matriz(2, 2);
    Matriz d = new Matriz(3, 2);
     
    double[][] temp;
        
    // Matriz "a"
    temp = a.getMatriz();
    // 1a. linha
    temp[0][0] = 1.0;
    temp[0][1] = 1.0;
        
    // 2a. linha
    temp[1][0] = 1.0;
    temp[1][1] = 1.0;

        
    // Matriz "b"
    temp = b.getMatriz();
    // 1a. linha
    temp[0][0] = 2.0;
    temp[0][1] = 2.0;
      
    // 2a. linha
    temp[1][0] = 2.0;
    temp[1][1] = 2.0;
    
    
    // Matriz "c"
    temp = c.getMatriz();
    // 1a. linha
    temp[0][0] = 1.0;
    temp[0][1] = 1.0;
      
    // 2a. linha
    temp[1][0] = 1.0;
    temp[1][1] = 1.0;
    
    
    // Matriz "d"
    temp = d.getMatriz();
    // 1a. linha
    temp[0][0] = 2.0;
    temp[0][1] = 2.0;
    
    // 2a. linha
    temp[1][0] = 2.0;
    temp[1][1] = 2.0;
      
    // 3a. linha
    temp[2][0] = 2.0;
    temp[2][1] = 2.0;
      
    System.out.println("Matriz \"a\": " + a);
    System.out.println("Matriz \"b\": " + b);
    System.out.println("Matriz \"c\": " + c);
    System.out.println("Matriz \"d\": " + d);    
        
    System.out.println("---------------------------------------------------------");
    try {
      System.out.println("\"a\" + \"b\".: " + a.soma(b));
      System.out.println("\"a\" X \"b\".: " + a.prod(b));
      System.out.println("\"c\" + \"d\".: " + c.soma(d));
      System.out.println("\"c\" X \"d\".: " + c.prod(d)); 
    } catch (MatrizInvalidaException | SomaMatrizesIncompativeisException | ProdMatrizesIncompativeisException err) {
        System.out.println("Erro: " + err.getLocalizedMessage()); 
      }
    }  
}